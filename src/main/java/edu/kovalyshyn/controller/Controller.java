package edu.kovalyshyn.controller;

public interface Controller {
    void task1();
    void task2();
    void task3();
    void task4();

}
