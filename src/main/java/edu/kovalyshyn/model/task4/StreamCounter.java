package edu.kovalyshyn.model.task4;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamCounter {

    public static List<String> sortedUniqueWords(List<String> uniqueWords) {
        return uniqueWords.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public static Map<String, Long> wordCount(List<String> words) {
        return words.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    public static Map<Character, Long> charsCount(List<String> words) {
        List<Character> chars = new ArrayList<>();
        for (String st : words) {
            for (Character c : st.toLowerCase().toCharArray()) {
                chars.add(c);
            }
        }

        return chars.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

}
