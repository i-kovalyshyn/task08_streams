package edu.kovalyshyn.model;

public interface Model {
    void task1();
    void task2();
    void task3();
    void task4();
}
