package edu.kovalyshyn.model.task2_command.command;

import edu.kovalyshyn.model.task2_command.receiver.TextFile;

public class ReadTextFileOperation implements TextFileOperation {
    private TextFile textFile;

    public ReadTextFileOperation(TextFile textFile) {
        this.textFile = textFile;
    }
    @Override
    public String execute() {
        return textFile.read();
    }
}
