package edu.kovalyshyn.model.task2_command.command;

import edu.kovalyshyn.model.task2_command.receiver.TextFile;
import lombok.Data;

@Data
public class OpenTextFileOperation implements TextFileOperation {
    private TextFile textFile;

    public OpenTextFileOperation(TextFile textFile) {
        this.textFile = textFile;
    }

    @Override
    public String execute() {
        return textFile.open();
    }
}
