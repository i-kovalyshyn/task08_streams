package edu.kovalyshyn.model.task2_command.command;

@FunctionalInterface
public interface TextFileOperation {
    String execute();
}
