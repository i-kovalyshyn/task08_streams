package edu.kovalyshyn.model.task2_command.command;

import edu.kovalyshyn.model.task2_command.receiver.TextFile;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class SaveTextFileOperation implements TextFileOperation {
    private TextFile textFile;

    public SaveTextFileOperation(TextFile textFile) {
        this.textFile = textFile;
    }

    @Override
    public String execute() {
        return textFile.save();
    }
}
