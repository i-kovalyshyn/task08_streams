package edu.kovalyshyn.model.task2_command.receiver;

import lombok.Data;

@Data
public class TextFile {
    private String name;

    public TextFile(String name) {
        this.name = name;
    }

    public String open(){
        return "Opening file" + name;
    }
    public String save(){
        return "Saving file" + name;
    }

    public String read() {
        return "Reading file " + name;
    }

    public String write() {
        return "Writing to file " + name;
    }
}
