package edu.kovalyshyn.model;

import edu.kovalyshyn.model.task1.MyInterface;
import edu.kovalyshyn.model.task2_command.command.ReadTextFileOperation;
import edu.kovalyshyn.model.task2_command.command.TextFileOperation;
import edu.kovalyshyn.model.task2_command.invoker.TextFileOperationExecutor;
import edu.kovalyshyn.model.task2_command.receiver.TextFile;
import edu.kovalyshyn.model.task3.StatisticsOfList;
import edu.kovalyshyn.model.task4.StreamCounter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Stream;

public class ModelImpl implements Model {
    private static Logger log = LogManager.getLogger(ModelImpl.class);

    @Override
    public void task1() {
        MyInterface myInterface = ((a, b, c) -> Stream.of(a, b, c)
                .max(Integer::compareTo).get());
        log.info("MAX value of numbers [10,50,15] is - " + myInterface.foo(10, 50, 15));

        myInterface = (a, b, c) -> (int) Stream.of(a, b, c)
                .mapToInt(i_ -> i_)
                .average()
                .orElse(-1);
        log.info("Average of numbers [3,7,8] is - " + myInterface.foo(3, 7, 8));

    }

    @Override
    public void task2() {
        TextFileOperationExecutor textFileOperationExecutor
                = new TextFileOperationExecutor();
        log.info(textFileOperationExecutor.executeOperation(() -> "Opening file file1.txt"));

        TextFile textFile = new TextFile("file2.txt");
        log.info(textFileOperationExecutor.executeOperation(textFile::save));

        TextFileOperation readTextFile = new ReadTextFileOperation(new TextFile("file3.txt"));
        log.info(textFileOperationExecutor.executeOperation(readTextFile));


    }

    @Override
    public void task3() {
        StatisticsOfList statisticsOfList = new StatisticsOfList();
        log.info(statisticsOfList.statisticsOfList(10, 0, 30));
        log.info("Count of values bigger than average value: "
                + statisticsOfList.countValuesBiggerAvg());
    }

    public static List<String> readWord(Scanner source) {
        String word = null;
        List<String> words = new ArrayList<>();
        while (!(word = source.nextLine()).isEmpty()) {
            words.add(word);
            System.out.print("input next, ");
            System.out.println("ore press Enter to finish inputting");
        }
        return words;
    }

    @Override
    public void task4() {
        System.out.println("Please, input something:");
        List<String> wordList = readWord(new Scanner(System.in));
        long uniqueWordsCount = wordList.stream().distinct().count();
        log.info("Unique words count = " + uniqueWordsCount);

        List<String> sortedUniqueWords = StreamCounter.sortedUniqueWords(wordList);
        log.info("Sorted list of unique words: " + sortedUniqueWords);

        Map<String, Long> wordCount = StreamCounter.wordCount(wordList);
        log.info("Word count:" + wordCount);

        Map<Character, Long> charCount = StreamCounter.charsCount(wordList);
        log.info("Characters count: " + charCount);
        charCount.entrySet().forEach(System.out::println);

    }
}
