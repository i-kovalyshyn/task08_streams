package edu.kovalyshyn.model.task3;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.OptionalDouble;
import java.util.Random;
import java.util.stream.Collectors;

public class StatisticsOfList {
    private static List<Integer> generateList(int limit, int low, int high) {
        return new Random().ints(limit, low, high)
                .boxed()
                .collect(Collectors.toList());
    }

    public IntSummaryStatistics statisticsOfList(int limit, int low, int high) {
        return generateList(limit, low, high).stream()
                .mapToInt(x -> x)
                .summaryStatistics();
    }

    public long countValuesBiggerAvg() {
        List<Integer> list = generateList(10, 0, 50);
       double avg = list.stream()
                .mapToInt(i->i)
                .average()
               .orElse(-1);
       return list.stream()
               .filter(i->i>avg).count();

    }
}
