package edu.kovalyshyn.view;

import edu.kovalyshyn.controller.Controller;
import edu.kovalyshyn.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger log = LogManager.getLogger(MyView.class);


    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - Create functional interface  - task 1 ");
        menu.put("2", " 2 - Implement pattern Command task -2");
        menu.put("3", " 3 - IntSummaryStatistics task 3");
        menu.put("4", " 4 - Word counter task 4");
        menu.put("Q", " Q - EXIT");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);

    }

    private void pressButton1() {
        controller.task1();
    }

    private void pressButton2() {
        controller.task2();
    }

    private void pressButton3() {
        controller.task3();
    }

    private void pressButton4() {
        controller.task4();

    }

    private void outputMenu() {
        System.out.println("\n MENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                log.info("-EXIT- ");
            }
        } while (!keyMenu.equals("Q"));
    }
}
